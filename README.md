## Backstage POC

#### Run instructions
1. Clone This Repo
2. Search for BackstagePOC Bitbucket on OnePassword and copy it
3. Edit ```backstage/app-config.yaml``` and override the bitBucket app password with the one previously copied (around line 50)
4. On the main folder run ```docker-compose up --build```
5. It takes some minutes. At the end backstage is served on ```localhost:7007```

#### Create a microservice instructions

1. On backstage main page click Create... on the left side menu
2. Choose the FastAPI template
3. Fill the form with requested values (similar to our cookiecutter script). 
4. Use only lowercase chars for the microservice name
5. When asked for bitbucket infos put this values:
    - for workspace: mattias_santoro
    - for project: GENERIC
    - repository: a valid repo name
6. Confirm and check all steps pass.

#### Expected result

1. Created service is available on the backstage catalog
2. A repo with the service code and catalog info is created on mattias_santoro/GENERIC bitbucket workspace